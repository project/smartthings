<?php

namespace Drupal\smartthings\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'smartthings_default' formatter.
 *
 * @FieldFormatter(
 *   id = "smartthings_default",
 *   label = @Translation("SmartThings Default"),
 *   field_types = {
 *     "smartthings_capabilities"
 *   }
 * )
 */
class SmartThingsDefaultFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      // Implement default settings.
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return [
      // Implement settings form.
    ] + parent::settingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    // Implement settings summary.
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $elements[$delta] = ['#markup' => $this->viewValue($item)];
    }

    return $elements;
  }

  /**
   * Generate the output appropriate for one field item.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   One field item.
   *
   * @return string
   *   The textual output generated.
   */
  protected function viewValue(FieldItemInterface $item) {
    // Load options array with device capability list from the database.
    $connection = \Drupal::database();
    $resultObj = $connection->query("SELECT value, updated_datetime FROM {smartthings_devices} WHERE id = :id", [':id' => $item->value])->fetchObject();

    if ($resultObj->value) {
      $value = $resultObj->value;
      $unit = ' ' . $item->unit;
      return nl2br(Html::escape($value . $unit));
    }
    else {
      return nl2br(t("The device capability is not available because the value hasn't been updated by the SmartThings hub, or the device was deleted from the Drupal Interface SmartApp."));
    }
  }

}
