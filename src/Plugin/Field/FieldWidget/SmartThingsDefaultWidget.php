<?php

namespace Drupal\smartthings\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'smartthings_default' widget.
 *
 * @FieldWidget(
 *   id = "smartthings_default",
 *   label = @Translation("SmartThings default"),
 *   field_types = {
 *     "smartthings_capabilities"
 *   }
 * )
 */
class SmartThingsDefaultWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    // Load options array with device list from database.
    $connection = \Drupal::database();
    $query = $connection->select('smartthings_devices', 's');
    $query->addField('s', 'id');
    $query->addExpression('concat(label, :dash, component_id, :dash, capability_id, :dash, COALESCE(value, :empty))', 'device_capability', [':empty' => 'No Value', ':dash' => ' - ']);
    $result = $query->execute();
    $options = $result->fetchAllKeyed(0, 1);

    // Default to the set value or use NULL value.
    $value = isset($items[$delta]->value) ? $items[$delta]->value : NULL;

    $element['value'] = [
      '#title' => $this->t('Device Capability'),
      '#type' => 'select',
      '#description' => t('Select a device capability from the list.'),
      '#default_value' => $value,
      '#options' => $options,
      '#empty_option' => t('- None -'),
      '#empty_value' => '',

    ];
    $element['unit'] = [
      '#title' => $this->t('Unit of Measurement'),
      '#type' => 'textfield',
      '#default_value' => isset($items[$delta]->unit) ? $items[$delta]->unit : NULL,
    ];
    return $element;
  }

}
