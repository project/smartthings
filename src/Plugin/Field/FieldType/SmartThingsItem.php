<?php

namespace Drupal\smartthings\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'smartthings' field type.
 *
 * @FieldType(
 *   id = "smartthings_capabilities",
 *   label = @Translation("SmartThings Device Capabilities"),
 *   description = @Translation("This field stores a linked SmartThings device capability in the database."),
 *   default_widget = "smartthings_default",
 *   default_formatter = "smartthings_default"
 * )
 */
class SmartThingsItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field) {
    return [
      'columns' => [
        'value' => [
          'type' => 'text',
          'size' => 'big',
          'not null' => FALSE,
        ],
        'unit' => [
          'type' => 'varchar',
          'length' => 256,
          'not null' => FALSE,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('value')->getValue();
    return $value === NULL || $value === '';
  }

  /**
   * {@inheritdoc}
   */
  public static $propertyDefinitions;

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {

    $properties['value'] = DataDefinition::create('string')
      ->setLabel(t('SmartThings Device Capability'));

    $properties['unit'] = DataDefinition::create('string')
      ->setLabel(t('Unit'))
      ->setDescription(t('Unit of measurement to use with the device capability value.'));

    return $properties;
  }

}
