<?php

namespace Drupal\smartthings\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class WebhookController.
 */
class WebhookController extends ControllerBase {

  /**
   * Drupal\Core\Logger\LoggerChannelFactory definition.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $logger;

  /**
   * The configuration object factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Enable or disable debugging.
   *
   * @var bool
   */
  protected $debug = FALSE;

  /**
   * Constructs a new WebhookController object.
   *
   * @param \Drupal\Core\Logger\LoggerChannelFactory $logger
   *   The logger channel factory.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The configuration object factory.
   */
  public function __construct(LoggerChannelFactory $logger, ConfigFactoryInterface $configFactory) {
    $this->configFactory = $configFactory;
    $this->logger = $logger->get('Smartthings');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('logger.factory'),
      $container->get('config.factory'),
    );
  }

  /**
   * Capture the payload.
   *
   * @return Symfony\Component\HttpFoundation\Response
   *   A simple string and 200 response.
   */
  public function capture(Request $request) {
    // Keep things fast.
    // Don't load a themed site for the response.
    // Most Webhook providers just want a 200 response.
    $response = new Response();

    // $r = json_decode(file_get_contents('php://input'));
    // $req_dump = print_r( $r, true );
    // $fp = file_put_contents( 'request.log', $req_dump );
    // Capture the payload.
    // Option 2: $payload = file_get_contents("php://input");.
    $payload = $request->getContent();

    // Get the authorization header.
    $request_header_auth = $request->headers->get('authorization');
    // Get the headers variable so we know what headers are included in
    // the "signing string".
    $signature = "";
    $signing_string = "";
    if (isset($request_header_auth)) {
      $sign_headers = strstr(substr(strstr($request_header_auth, 'headers="'), 9), '"', TRUE);
      $sign_headers = explode(" ", $sign_headers);

      foreach ($sign_headers as &$sign_head) {

        // Add special request target value if it is listed in the signing
        // string headers.
        if ($sign_head == "(request-target)") {

          $signing_string .= "(request-target): post /smartthings/webhook\n";
        }
        else {
          // Add each header.
          $signing_string .= $sign_head . ": " . $request->headers->get($sign_head) . "\n";
        }

      }
      // Remove newline at the end of the string.
      $signing_string = rtrim($signing_string);

      // Get the signature from the authorization variable from the
      // request header.
      $signature = strstr(substr(strstr($request_header_auth, 'signature="'), 11), '"', TRUE);
    }

    // $fp = file_put_contents('request.log', print_r($signing_string, true));
    // Check if it is empty.
    if (empty($payload)) {
      $message = 'The payload was empty.';
      $this->logger->error($message);
      $response->setContent($message);
      return $response;
    }

    // Use to inspect the payload.
    if ($this->debug) {
      $this->logger->debug('<pre>@payload</pre>', ['@payload' => $payload]);
    }

    // Make the payload into an Object.
    $payloadObj = json_decode($payload);

    // We don't yet have the public key during PING (when the app is created),
    // so no need to verify the signature. All other requests are verified.
    // Process if PING or verify then process.
    if ($payloadObj->lifecycle === "PING" || $this->signatureIsVerified($signing_string, $signature)) {
      $response->setContent($this->handleRequest($payloadObj));

      // Inspect the response.
      if ($this->debug) {
        $this->logger->debug('<pre>@response</pre>', ['@response' => $response]);
      }
      return $response;

    }
    else {
      // Send unauthorized message and code.
      $message = 'Forbidden.';
      $this->logger->error($message);
      $response->setContent($message);
      $response->setStatusCode(Response::HTTP_UNAUTHORIZED);

      // Inspect the response.
      if ($this->debug) {
        $this->logger->debug('<pre>@response</pre>', ['@response' => $response]);
      }
      return $response;
    }
  }

  /**
   * Verifies that the request is actually from SmartThings.
   *
   * @returns true if verified, false otherwise.
   */
  public function signatureIsVerified($signingStr, $sig) {

    $config_settings = $this->configFactory->get('smartthings.settings');

    $public_key = $config_settings->get('smartthings.public_key');
    if ($public_key == "") {
      $this->logger->error('The Public Key is not set.  See SmartThings Settings page or Help for more info.');
    }

    $sig = base64_decode($sig, FALSE);
    // Check signature.
    $valid = openssl_verify($signingStr, $sig, $public_key, "RSA-SHA256");
    if ($valid == 1) {
      return TRUE;
    }
    elseif ($valid == 0) {
      return FALSE;
    }
    else {
      // ToDo throw error.
    }
  }

  /**
   * Process the webhook payload.
   *
   * @param object $payObj
   *   Payload as an object.
   *
   * @return jsonResponse
   *   JSON formatted response.
   */
  protected function handleRequest($payObj) {
    $lifecycle = $payObj->lifecycle;

    // URL to send app requests to when making INSTALL an UPDATE requests.
    $stApi = 'https://api.smartthings.com/v1';

    switch ($lifecycle) {
      // PING happens during app creation. Purpose is to verify app
      // is alive and is who it says it is.
      case 'PING':
        $this->logger->debug('ping');
        // {statusCode: 200, pingData: {challenge: $chal}});
        $chal = $payObj->pingData->challenge;

        $ping = new \stdClass();
        $ping->challenge = $chal;

        $resObj = new \stdClass();
        $resObj->pingData = $ping;

        $jsonResponse = json_encode($resObj);
        return $jsonResponse;

      // CONFIGURATION happens as user begins to install the app.
      case 'CONFIGURATION':
        $config = \Drupal::config('smartthings.settings');
        $phase = $payObj->configurationData->phase;

        $this->logger->debug('config');

        $confData = new \stdClass();
        switch ($phase) {
          case 'INITIALIZE':
            $initialize = $config->get('createConfiguration.initialize');
            $confData->initialize = $initialize;
            break;

          case 'PAGE':
            $page = $config->get('createConfiguration.page');
            $confData->page = $page;
            break;

          default:
            // Throw new Error(`Unsupported config phase: ${phase}`);.
            $this->logger->debug('<pre>@phase</pre>', ['@phase' => $phase]);
            break;
        }

        // response.json({statusCode: 200, configurationData: $res});.
        $resObj = new \stdClass();
        $resObj->configurationData = $confData;

        $jsonResponse = json_encode($resObj);
        $this->logger->debug('<pre>@jsonResponse</pre>', ['@jsonResponse' => $jsonResponse]);
        return $jsonResponse;

      // INSTALL happens after a user finishes configuration, and installs the
      // app.
      case 'INSTALL':

        // Once the user has selected the device and agreed to the requested
        // permissions, this app will create a subscription for the every value
        // change of any attribute for device that is selected.
        $token = $payObj->installData->authToken;
        $installedAppId = $payObj->installData->installedApp->installedAppId;
        $drupalDeviceList = $payObj->installData->installedApp->config->drupalDeviceList;

        $mime_type = "application/json";

        $client = \Drupal::httpClient();
        $url = $stApi . "/installedapps/" . $installedAppId . "/subscriptions";

        $delete_options = [
          'headers' => $token !== FALSE ? [
            'Authorization' => 'Bearer ' . $token,
          ] : [
            'Content-Type' => $mime_type,
          ],
        ];

        $delete_response = $client->delete($url, $delete_options);
        $i = 0;
        foreach ($drupalDeviceList as $drupalDevice) {
          $i++;
          $deviceConfig = $drupalDevice->deviceConfig;

          $body = '{
			      "sourceType": "DEVICE",
			      "device": {
			        "componentId": "' . $deviceConfig->componentId . '",
			        "deviceId": "' . $deviceConfig->deviceId . '",
			        "capability": "*",
			        "attribute": "*",
			        "stateChangeOnly": true,
			        "subscriptionName": "drupal_subscription' . $i . '",
			        "value": "*"
			      }
			    }';

          $options = [
            'headers' => $token !== FALSE ? [
              'Authorization' => 'Bearer ' . $token,
            ] : [
              'Content-Type' => $mime_type,
            ],
            'body' => $body,
          ];

          // Inspect the options.
          if ($this->debug) {
            $this->logger->debug('<pre>@body</pre>', ['@body' => $body]);
          }

          $client_response = $client->post($url, $options);

          // Get more info about the device (i.e. device name and capabilities)
          $getDeviceRequest = $client->get($stApi . "/devices/" . $deviceConfig->deviceId, $delete_options);
          $getDeviceResponse = json_decode($getDeviceRequest->getBody()->getContents());
          // If ($this->debug) {.
          if (FALSE) {
            $this->logger->debug('<pre>@getDeviceResponse</pre>', ['@getDeviceResponse' => $getDeviceResponse]);
          }

          // Add the device and capabilities to the Drupal DB smartthings table.
          $connection = \Drupal::database();

          foreach ($getDeviceResponse->components as $component) {
            foreach ($component->capabilities as $capability) {

              $result = $connection->insert('smartthings_devices')
                ->fields([
                  'st_device_id' => $getDeviceResponse->deviceId,
                  'name' => $getDeviceResponse->name,
                  'label' => $getDeviceResponse->label,
                  'component_id' => $component->id,
                  'capability_id' => $capability->id,
                ])
                ->execute();
            }
          }
        }

        $jsonResponse = '{"installData": {}}';
        return $jsonResponse;

      // UPDATE happens when a user updates the configuration of an
      // already-installed app.
      case 'UPDATE':

        // Remove the device row(s) from the Drupal DB smartthings table.
        $connection = \Drupal::database();
        $prevDeviceList = $payObj->updateData->previousConfig->drupalDeviceList;
        foreach ($prevDeviceList as $prevDevice) {
          $device_deleted = $connection->delete('smartthings_devices')
            ->condition('st_device_id', $prevDevice->deviceConfig->deviceId)
            ->execute();
        }

        $token = $payObj->updateData->authToken;
        $installedAppId = $payObj->updateData->installedApp->installedAppId;
        $drupalDeviceList = $payObj->updateData->installedApp->config->drupalDeviceList;

        $mime_type = "application/json";

        $client = \Drupal::httpClient();
        $url = $stApi . "/installedapps/" . $installedAppId . "/subscriptions";

        $delete_options = [
          'headers' => $token !== FALSE ? [
            'Authorization' => 'Bearer ' . $token,
          ] : [
            'Content-Type' => $mime_type,
          ],
        ];

        $delete_response = $client->delete($url, $delete_options);
        $i = 0;
        foreach ($drupalDeviceList as $drupalDevice) {
          $i++;
          $deviceConfig = $drupalDevice->deviceConfig;

          $body = '{
			      "sourceType": "DEVICE",
			      "device": {
			        "componentId": "' . $deviceConfig->componentId . '",
			        "deviceId": "' . $deviceConfig->deviceId . '",
			        "capability": "*",
			        "attribute": "*",
			        "stateChangeOnly": true,
			        "subscriptionName": "drupal_subscription' . $i . '",
			        "value": "*"
			      }
			    }';

          $options = [
            'headers' => $token !== FALSE ? [
              'Authorization' => 'Bearer ' . $token,
            ] : [
              'Content-Type' => $mime_type,
            ],
            'body' => $body,
          ];

          // Inspect the options.
          if ($this->debug) {
            $this->logger->debug('<pre>@body</pre>', ['@body' => $body]);
          }

          $client_response = $client->post($url, $options);

          // Get more info about the device (i.e. device name and capabilities)
          $getDeviceRequest = $client->get($stApi . "/devices/" . $deviceConfig->deviceId, $delete_options);
          $getDeviceResponse = json_decode($getDeviceRequest->getBody()->getContents());
          // If ($this->debug) {.
          if (FALSE) {
            $this->logger->debug('<pre>@getDeviceResponse</pre>', ['@getDeviceResponse' => $getDeviceResponse]);
          }

          // Add the device and capabilities to the Drupal DB smartthings table.
          $connection = \Drupal::database();

          foreach ($getDeviceResponse->components as $component) {
            foreach ($component->capabilities as $capability) {

              $result = $connection->insert('smartthings_devices')
                ->fields([
                  'st_device_id' => $getDeviceResponse->deviceId,
                  'name' => $getDeviceResponse->name,
                  'label' => $getDeviceResponse->label,
                  'component_id' => $component->id,
                  'capability_id' => $capability->id,
                ])
                ->execute();
            }
          }
        }

        $jsonResponse = '{"updateData": {}}';
        return $jsonResponse;

      // UNINSTALL happens when a user uninstalls the app.
      case 'UNINSTALL':

        // Remove all devices from the Drupal DB smartthings table.
        $connection = \Drupal::database();
        $all_devices_deleted = $connection->delete('smartthings_devices')
          ->execute();

        // response.json({statusCode: 200, uninstallData: {}});.
        $jsonResponse = '{"uninstallData": {}}';
        return $jsonResponse;

      // EVENT happens when any subscribed-to event or schedule executes.
      case 'EVENT':
        // handleEvent(evt.eventData);.
        $eventType = $payObj->eventData->events[0]->eventType;
        $deviceEvent = $payObj->eventData->events[0]->deviceEvent;

        switch ($eventType) {
          case 'DEVICE_EVENT':
            // Update the capabiltiy value on the Smartthings device table
            // when the subscription detects a change.
            $component = $deviceEvent->componentId;
            $capability = $deviceEvent->capability;
            $val = $deviceEvent->value;

            if ($this->debug) {
              $this->logger->debug('@val', ['@val' => $val]);
            }

            // Update device capability value from the Drupal DB
            // smartthings table.
            $connection = \Drupal::database();
            $deviceValueUpdated = $connection->update('smartthings_devices')
              ->fields([
                'value' => $val,
            // 'unit' => 'kWh',
                'updated_datetime' => date("Y-m-d H:i:s"),
              ])
              ->condition('st_device_id', $deviceEvent->deviceId, '=')
              ->condition('component_id', $component, '=')
              ->condition('capability_id', $capability, '=')
              ->execute();

            break;

          default:
            $this->logger->error('This app only expects DEVICE_EVENTs. Got @eventType', ['@eventType' => $eventType]);

        }

        // response.json({statusCode: 200, eventData: {}});.
        $jsonResponse = '{"eventData": {}}';
        return $jsonResponse;

      default:
        $this->logger->debug('<pre>@lifecycle</pre>', ['@lifecycle' => $lifecycle]);

    }
  }

}
