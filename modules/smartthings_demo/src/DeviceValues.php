<?php

namespace Drupal\smartthings_demo;

/**
 * Provides sample data for smartthings_demo module.
 */
class DeviceValues {

  /**
   * Returns array of sample records for demo purposes.
   *
   * @return array
   *   Array of sample records.
   *
   * @see smartthings_demo_install()
   */
  public static function getSampleItems() {
    return [
      [
        'st_device_id' => '00000000-0000-0000-0000-00000000001',
        'name' => 'Iris Smart Plug',
        'label' => 'Bedroom Lamp',
        'component_id' => 'main',
        'capability_id' => 'switch',
        'value' => 'on',
        'updated_datetime' => '2019-09-24 11:11:13',
      ],
      [
        'st_device_id' => '00000000-0000-0000-0000-00000000001',
        'name' => 'Iris Smart Plug',
        'label' => 'Bedroom Lamp',
        'component_id' => 'main',
        'capability_id' => 'configuration',
      ],
      [
        'st_device_id' => '00000000-0000-0000-0000-00000000001',
        'name' => 'Iris Smart Plug',
        'label' => 'Bedroom Lamp',
        'component_id' => 'main',
        'capability_id' => 'powerMeter',
        'value' => '1.1',
        'updated_datetime' => '2019-09-24 11:11:13',
      ],
      [
        'st_device_id' => '00000000-0000-0000-0000-00000000001',
        'name' => 'Iris Smart Plug',
        'label' => 'Bedroom Lamp',
        'component_id' => 'main',
        'capability_id' => 'energyMeter',
        'value' => '73.4',
        'updated_datetime' => '2019-09-24 11:11:13',
      ],
      [
        'st_device_id' => '00000000-0000-0000-0000-00000000002',
        'name' => 'Aeotec Dual Nano Switch 1',
        'label' => 'Chicken Door',
        'component_id' => 'main',
        'capability_id' => 'switch',
        'value' => 'on',
        'updated_datetime' => '2019-09-24 11:13:01',
      ],
      [
        'st_device_id' => '00000000-0000-0000-0000-00000000002',
        'name' => 'Aeotec Dual Nano Switch 1',
        'label' => 'Chicken Door',
        'component_id' => 'main',
        'capability_id' => 'actuator',
      ],
      [
        'st_device_id' => '00000000-0000-0000-0000-00000000003',
        'name' => 'Mobile Presence',
        'label' => 'Bobs Android',
        'component_id' => 'main',
        'capability_id' => 'presenceSensor',
        'value' => 'present',
        'updated_datetime' => '2019-09-24 12:33:05',
      ],
      [
        'st_device_id' => '00000000-0000-0000-0000-00000000004',
        'name' => 'Home Energy Meter (Gen5)',
        'label' => 'Pump Energy Meter',
        'component_id' => 'main',
        'capability_id' => 'powerMeter',
        'value' => '3842.3',
        'updated_datetime' => '2019-09-24 12:22:34',
      ],
      [
        'st_device_id' => '00000000-0000-0000-0000-00000000005',
        'name' => 'Local Weather Station MU',
        'label' => 'Weather Station',
        'component_id' => 'main',
        'capability_id' => 'relativeHumidityMeasurement',
        'value' => '9',
        'updated_datetime' => '2019-09-24 12:53:25',
      ],
      [
        'st_device_id' => '00000000-0000-0000-0000-00000000006',
        'name' => 'Zen Thermostat',
        'label' => 'Zen Thermostat',
        'component_id' => 'main',
        'capability_id' => 'temperatureMeasurement',
        'value' => '76',
        'updated_datetime' => '2019-09-24 12:42:15',
      ],
    ];
  }

}
