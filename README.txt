SmartThings Drupal Module README
===========

A SmartThings SmartApp for Drupal integration of device values.

Requirements
------------
  * Login access to a SmartThings SmartPhone App (Not the "SmartThings Classic"
    app) that is connected to a SmartThings hub with enabled devices. 
    https://www.smartthings.com/getting-started
  	* Alternatively install the SmartThings Demo module to populate the DB with
      fake SmartThings device values.
  * A SmartThings developer workspace: https://smartthings.developer.samsung.com/workspace/


Instructions
------------

Install the SmartThings module on your Drupal instance:

	1. Install/Enable SmartThings module
  2. Visit admin/config/smartthings/settings

Get the Public Key for your SmartThings SmartApp API connection:

  1. Login to your SmartThings developer workspace 
     (https://smartthings.developer.samsung.com/workspace/) and create a new project.
	2. Create an "Automation for the SmartThings App" and give it a name.
	3. Select Developer->Automation Connector->WebHook Endpoint
	4. Enter your target URL and use the "smartthings/webhook" path (eg. 
       https://example.com/smartthings/webhook)
	5. Add an "App Display Name" like "Drupal Integration" that you will see on the
       SmartPhone SmartThings app in the list of Automations.
	6. Select "r:devices:*" from the permissions scope list
	7. Click Next and Save
	8. Copy the resulting Public Key
	
Add the Public Key to your Drupal SmartApp:
	
	1. Visit admin/config/services/smartthings/settings
	2. Paste the Public Key from the SmartThings developer workspace
	3. Save
	
Add devices to your SmartThings SmartApp.

	1. Install your SmartThings SmartPhone App. (Note: the SmartThings Classic
       app will not work)
	2. Launch the SmartThings app. Your SmartThings app and Developer
       Workspace must be signed into the same account.
  3. Enable Developer Mode in the SmartThings app:
     * Launch the SmartThings app.
     * Go to Dashboard > Settings.
     * Long-press "About SmartThings" for 20 seconds.
     * Enable Developer Mode.
     * Restart the SmartThings app.
  4. Go to the Automations tab and click ADD AUTOMATION.
  5. Tap on the Automation app to install it.
